package com.bmstu;

import Jama.Matrix;

import java.util.ArrayList;

public class LR7Main {

    private final static double EPS = 0.01;

    private final static double a = -10;
    private final static double b = 15.0 / 4;
    private final static double c = 10;
    private final static double d = -4;
    private final static double e = -8;
    private final static ArrayList<Double> listOfGameCosts = new ArrayList<>();

    public static void main(String[] args) {
        CoreFunction coreFunction = (x, y) -> a * x * x + b * y * y + c * x * y + d * x + e * y;
        double lastH = 0;

        for (int i = 3; i < 1000; ++i) {
            double[][] approximateGrid = aproximateGrid(i, coreFunction);
            System.out.println("N=" + i);
            Matrix approximateMatrix = new Matrix(approximateGrid);
            if (i < 12) {
                approximateMatrix.print(3, 3);
            }

            BrawnRobinson solver = new BrawnRobinson(approximateGrid, EPS);
            MixedGameStrategy.Results results = solver.solution();

            double x = getIndexOfMaxProbableStrategy(results.getStrategyForX()) / i;
            double y = getIndexOfMaxProbableStrategy(results.getStrategyForY()) / i;
            double h = coreFunction.value(x, y);

            listOfGameCosts.add(h);

            System.out.println("X:" + x + " " +
                    "Y:" + y + " " +
                    "H:" + h);

            if (i > 11 && checkEpsForStopping(EPS)) {
                return;
            }
        }
    }

    private static boolean checkEpsForStopping(double eps) {
        double max = (-1) * Double.MAX_VALUE;
        double min = Double.MAX_VALUE;

        for (int i = listOfGameCosts.size() - 10; i < listOfGameCosts.size(); ++i) {
            if (listOfGameCosts.get(i) > max) {
                max = listOfGameCosts.get(i);
            }
            if (listOfGameCosts.get(i) < min) {
                min = listOfGameCosts.get(i);
            }
        }
        System.out.println("max:" + max + " min" + min + " eps:" + Math.abs(max - min));
        if (Math.abs(max - min) < eps) {
            return true;
        }

        return false;
    }

    private static double getIndexOfMaxProbableStrategy(double[] mixedStrategy) {
        double maxProbability = mixedStrategy[0];
        double index = 0;
        for (int i = 0; i < mixedStrategy.length; ++i) {
            if (mixedStrategy[i] > maxProbability) {
                maxProbability = mixedStrategy[i];
                index = i;
            }
        }
        return index;
    }

    private static double[][] aproximateGrid(int quantityOfCells, CoreFunction coreFunction) {
        double[][] result = new double[quantityOfCells][quantityOfCells];

        for (int i = 0; i < quantityOfCells; ++i) {
            for (int j = 0; j < quantityOfCells; ++j) {
                result[i][j] = coreFunction.value((double) i / quantityOfCells,
                        (double) j / quantityOfCells);
            }
        }

        return result;
    }

    private interface CoreFunction {
        double value(double x, double y);
    }
}
