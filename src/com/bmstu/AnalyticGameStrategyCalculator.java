package com.bmstu;

import Jama.Matrix;

public class AnalyticGameStrategyCalculator implements MixedGameStrategy {

    private double[][] gameMatrix;

    AnalyticGameStrategyCalculator(double[][] gameMatrix) {
        this.gameMatrix = gameMatrix;
    }

    @Override
    public Results solution() {
        return analyticSolution();
    }

    private Results analyticSolution() {
        double[] uRow = new double[gameMatrix.length];
        for (int i = 0; i < gameMatrix.length; ++i) {
            uRow[i] = 1;
        }
        double[][] u = {uRow};
        double[][] u_t = new double[gameMatrix.length][1];
        for (int i = 0; i < gameMatrix.length; ++i) {
            u_t[i][0] = 1;
        }
        System.out.println("Ut");
        new Matrix(u_t).print(1, 4);

        double[][] c = gameMatrix;
        double[][] c_reverse = MatrixTools.calculateReverseMatrix(c);

        double[][] c_reverseUT = MatrixTools.multiplicate(c_reverse, u_t);
        System.out.println("c^(-1)*Ut");
        new Matrix(c_reverseUT).print(1, 4);

        final double uCuT = MatrixTools.multiplicate(u, c_reverseUT)[0][0];

        System.out.println("uCuT:" + uCuT);

        return new Results() {
            @Override
            public double[] getStrategyForX() {
                return MatrixTools.multiplicateByConst(
                        MatrixTools.multiplicate(u, c_reverse),
                        1 / uCuT
                )[0];
            }

            @Override
            public double[] getStrategyForY() {
                double[][] yStrategy = MatrixTools.multiplicateByConst(
                        MatrixTools.multiplicate(c_reverse, u_t),
                        1 / uCuT
                );

                double[] y = new double[yStrategy.length];
                for (int i = 0; i < yStrategy.length; ++i) {
                    y[i] = yStrategy[i][0];
                }

                return y;
            }

            @Override
            public double getCost() {
                return 1 / uCuT;
            }
        };
    }
}
