package com.bmstu;

import java.io.*;
import java.util.Arrays;

public class LR6Main {

    private final static double EPS = 0.1;

    public static void main(String[] args) {
        double[][] inputMatrix = readMatrixFromFile("input.txt");

        MixedGameStrategy analyticGameStrategy = new AnalyticGameStrategyCalculator(inputMatrix);

        MixedGameStrategy.Results analyticSolutionResults = analyticGameStrategy.solution();
        System.out.println("\n\nAnalyticGameStrategyCalculator solution:\n\n");
        printResults(analyticSolutionResults);
        System.out.println("\n\n\n");

        System.out.println("\nBrawn-Robinson solution:");
        MixedGameStrategy brawnRobinsonGameStrategy = new BrawnRobinson(inputMatrix, EPS);
        MixedGameStrategy.Results brawnRobinsonResults = brawnRobinsonGameStrategy.solution();
        printResults(brawnRobinsonResults);
    }

    private static void printResults(MixedGameStrategy.Results results) {
        System.out.print("\nX:" + Arrays.toString(results.getStrategyForX()));
        System.out.println();

        System.out.print("Y:" + Arrays.toString(results.getStrategyForY()));
        System.out.println("\nCost of the game:" + results.getCost());
    }

    private static double[][] readMatrixFromFile(String inputFilePath) {
        /**     Input file format:
         *      N M
         *      A11 A12 A13 ... A1M
         *        ....
         *      AN1 AN2 AN3 ... ANM
         * */

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(inputFilePath));
            String line = reader.readLine();
            String[] dimension = line.split(" ");

            if (dimension.length != 2) {
                return null;
            }

            Integer dimX = Integer.valueOf(dimension[0]);
            Integer dimY = Integer.valueOf(dimension[1]);

            double[][] matrix = new double[dimX][dimY];
            for (int i = 0; i < dimX; ++i) {
                // Read Row
                String[] valuesInRow = reader.readLine().split(" ");
                if (valuesInRow.length != dimY) {
                    return null;
                }

                for (int j = 0; j < dimY; ++j) {
                    matrix[i][j] = Double.valueOf(valuesInRow[j]);
                }
            }

            return matrix;
        } catch (IOException e) {
            return null;
        }
    }
}
