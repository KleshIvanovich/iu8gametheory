package com.bmstu;

import java.text.DecimalFormat;
import java.text.Format;
import java.util.Arrays;

public class LR11Main {

//    private final static int[] input = {0, //0 0000 0
//                                        4, //1 0001 1
//                                        3, //2 0010 2
//                                        2, //3 0100 4
//                                        3, //4 1000 8
//                                        8, //5 0011 3
//                                        6, //6 0101 4
//                                        7, //7 1001 9
//                                        5, //8 0110 6
//                                        7, //9 1010 10
//                                        6, //10 1100 12
//                                        10,//11 0111 7
//                                        12,//12 1011 11
//                                        10,//13 1101 13
//                                        9, //14 1110 14
//                                        13};//15 1111 15

    private final static int NUMBER_OF_PLAYERS = 4;
    private final static int NUMBER_OF_SUBSETS = 1 << NUMBER_OF_PLAYERS;
    private final static int[] input = {0, //0 0000 0
            4, //1 0001 1
            3, //2 0010 2
            8, //5 0011 3
            2, //3 0100 4
            6, //6 0101 5
            5, //8 0110 6
            10,//11 0111 7
            3, //4 1000 8
            7, //7 1001 9
            7, //9 1010 10
            12,//12 1011 11
            6, //10 1100 12
            10,//13 1101 13
            9, //14 1110 14
            13};//15 1111 15

    private final static int[] correctedInput = {0, //0 0000 0
            4, //1 0001 1
            3, //2 0010 2
            8, //5 0011 3
            2, //3 0100 4
            6, //6 0101 5
            5, //8 0110 6
            10,//11 0111 7
            3, //4 1000 8
            7, //7 1001 9
            7, //9 1010 10
            12,//12 1011 11
            6, //10 1100 12
            10,//13 1101 13
            9, //14 1110 14
            15};//15 1111 15

    public static void main(String[] args) {
        checkForSuperAddity(input);
//        checkForConvenxity(input);
        checkForSuperAddity(correctedInput);
        checkForConvenxity(correctedInput);
        double[] sheplyVector = calculateSheplyVector(correctedInput);
        System.out.println("Вектор Шепли:");
        printVectorWithFormatting(sheplyVector);

        double sum = 0;
        for (double item : sheplyVector) {
            sum += item;
        }

        System.out.println("Сумма:" + sum + " v(I)=" + correctedInput[NUMBER_OF_SUBSETS - 1]);
    }

    private static void printVectorWithFormatting(double[] vector) {
        System.out.print("[");
        Format format = new DecimalFormat("#0.000");
        for (int i = 0; i < vector.length; ++i) {
            double item = vector[i];
            String toPrint = ((DecimalFormat) format).format(item);
            if (i == vector.length - 1) {
                System.out.print(toPrint);
                continue;
            }
            System.out.print(toPrint + ", ");
        }
        System.out.print("]\n");
    }

    private static boolean checkForSuperAddity(int[] input) {
        boolean result = true;
        for (int subsetI = 1; subsetI < NUMBER_OF_SUBSETS; subsetI++) {
            for (int subsetJ = 1; subsetJ < NUMBER_OF_SUBSETS; subsetJ++) {
                if ((subsetI & subsetJ) != 0) {
                    continue;
                }

                int union = subsetI | subsetJ;

                if (input[subsetI] + input[subsetJ] > input[union]) {
                    System.out.println("Свойство супераддидивности не выполняется для множеств:" + Integer.toBinaryString(subsetI) + " и " + Integer.toBinaryString(subsetJ));
                    result = false;
                }
            }
        }
        return result;
    }

    private static boolean checkForConvenxity(int[] input) {
        boolean result = true;

        for (int subsetI = 1; subsetI < NUMBER_OF_SUBSETS; subsetI++) {
            for (int subsetJ = 1; subsetJ < NUMBER_OF_SUBSETS; subsetJ++) {
                int union = input[subsetI & subsetJ];
                int intersection = input[subsetI | subsetJ];
                int sum = input[subsetI] + input[subsetJ];

                if (union + intersection < sum) {
                    System.out.println("Свойство выпуклости не выполняется для множеств:" + Integer.toBinaryString(subsetI) + " и " + Integer.toBinaryString(subsetJ));
                    result = false;
                }
            }
        }
        return result;
    }

    private static double[] calculateSheplyVector(int[] charFunction) {
        double[] sheplyVector = new double[NUMBER_OF_PLAYERS];
        Arrays.fill(sheplyVector, 0);

        for (int i = 0; i < NUMBER_OF_PLAYERS; ++i) {
            int playerMask = 1 << i;
            for (int subset = 1; subset < NUMBER_OF_SUBSETS; ++subset) {
                int sizeOfSubset = 0;
                for (int j = 0; j < NUMBER_OF_PLAYERS; ++j) {
                    int shift = 1 << j;
                    if ((shift & subset) != 0) {
                        sizeOfSubset++;
                    }
                }

                if ((subset & playerMask) == 0) {
                    continue;
                }

                int subsetWithoutPlayer = subset ^ playerMask;
                sheplyVector[i] += (factorial(sizeOfSubset - 1)) * factorial(NUMBER_OF_PLAYERS - sizeOfSubset) * (charFunction[subset] - charFunction[subsetWithoutPlayer]);
            }

            sheplyVector[i] /= (factorial(NUMBER_OF_PLAYERS));
        }

        return sheplyVector;
    }

    private static int factorial(int input) {
        int result = 1;

        for (int i = 1; i <= input; ++i) {
            result = i * result;
        }
        return result;
    }
}