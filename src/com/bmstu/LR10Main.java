package com.bmstu;

import guru.nidi.graphviz.attribute.Color;
import guru.nidi.graphviz.attribute.Label;
import guru.nidi.graphviz.attribute.RankDir;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.Graph;
import guru.nidi.graphviz.model.LinkSource;
import guru.nidi.graphviz.model.Node;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static guru.nidi.graphviz.model.Factory.*;

public class LR10Main {

    private final static int NUMBER_OF_PLAYERS = 3;
    private final static int DEPTH = 6;
    private final static int BOUNDARY = 50;

    public static void main(String[] args) throws IOException {
        InnerNode root = new InnerNode(null, 0, 0);
        InnerNode graph = PositionGameGraph.generateGraph(root, DEPTH, 2, BOUNDARY);
        graph.setPositionDescription(PositionGameGraph.getPositionDescriptionWithReverseInduction(graph));
        System.out.println("Best position:" + PositionGameGraph.arrayToString(graph.getPositionDescription()));
        PositionGameGraph.setColorsForOptimalStrategy(graph, Color.RED);
        PositionGameGraph.writeGraphToPng(graph, "example/ex4.png");
    }

    private static class PositionGameGraph {

        private static int vertexCounter = 0;

        private static InnerNode generateGraph(InnerNode root, int depth, int quantityOfEdges, int boundary) {
            vertexCounter++;
            if (depth == 0) {
                InnerNode node = new InnerNode(root, vertexCounter,depth + 1);
                node.setPositionDescription(generatePositionDescription(boundary));
                return node;
            }

            for (int i = 0; i < quantityOfEdges; ++i) {
                vertexCounter++;
                InnerNode child = new InnerNode(root, vertexCounter, depth);
                child = generateGraph(child, depth - 1, quantityOfEdges, boundary);
                root.addChildren(child);
            }

            return root;
        };

        private static void writeGraphToPng(InnerNode root, String filename) throws IOException {
            List<LinkSource> edgesAndVertex = getLinkSourceList(root);
            Graph g = graph("Дерево обратной индукции").directed()
                    .graphAttr().with(RankDir.TOP_TO_BOTTOM)
                    .with(
                           edgesAndVertex
                    );
            Graphviz.fromGraph(g).render(Format.PNG).toFile(new File(filename));
        }

        private static List<LinkSource> getLinkSourceList(InnerNode root) {
            List<LinkSource> linkSources = new LinkedList<>();
            if (root.getChildren().size() == 0) {
                return linkSources;
            }

            Node from = node(Integer.toString(root.getName()));
            if (root.getName() == 0) {
                from = node(Integer.toString(root.getName())).with(Label.lines(Label.Justification.MIDDLE, Integer.toString(root.getName())));
            }

            Edge firstChildNode = root.getChildren().get(0);
            String firstChild = Integer.toString(firstChildNode.getEnding().getName());
            Color firstChildColor = firstChildNode.getColor();
            String firstChildLabel = arrayToString(firstChildNode.getEnding().getPositionDescription());

            Node firstChildVertex = node(firstChild).with(Label.lines(Label.Justification.LEFT, firstChild, firstChildLabel));

            Edge secondChildNode = root.getChildren().get(1);
            String secondChild = Integer.toString(secondChildNode.getEnding().getName());
            Color secondChildColor = secondChildNode.getColor();
            String secondChildLabel = arrayToString(secondChildNode.getEnding().getPositionDescription());

            Node secondChildVertex = node(secondChild).with(Label.lines(Label.Justification.RIGHT, secondChild, secondChildLabel));

            linkSources.add(from.link(
                    to(firstChildVertex).with(firstChildColor),
                    to(secondChildVertex).with(secondChildColor)
            ));

            linkSources.addAll(getLinkSourceList(firstChildNode.getEnding()));
            linkSources.addAll(getLinkSourceList(secondChildNode.getEnding()));
            return linkSources;
        }

        private static InnerNode setColorsForOptimalStrategy(InnerNode root, Color color) {
            if (root.getChildren().size() == 0) {
                return root;
            }
            int[][] positions = root.getPositionDescription();
            for (Edge child : root.getChildren()) {
                for (int[] position : positions) {
                    for (int[] childPosition : child.getEnding().positionDescription) {
                        if (Arrays.equals(position, childPosition)) {
                            child.setColor(color);
                        }
                    }
                }

                setColorsForOptimalStrategy(child.getEnding(), child.getColor());
            }
            return root;
        }

        private static String arrayToString(int[][] arrays) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int[] array : arrays) {
                stringBuilder.append(Arrays.toString(array)).append("\n");
            }

            return stringBuilder.toString();
        }

        private static int[][] getPositionDescriptionWithReverseInduction(InnerNode root) {
            // We have a list
            if (root.getChildren().size() == 0) {
                return root.getPositionDescription();
            }

            int max = Integer.MIN_VALUE;
            int playerIndex = root.getDepth() % NUMBER_OF_PLAYERS;

            for (Edge child : root.getChildren()) {
                int[][] positionDescription = getPositionDescriptionWithReverseInduction(child.ending);
                for (int i = 0; i < positionDescription.length; ++i) {
                    if (positionDescription[i][playerIndex] > max) {
                        max = positionDescription[i][playerIndex];
                    }
                }
            }

            LinkedList<int[]> bestPositions = new LinkedList<>();

            for (Edge child : root.getChildren()) {
                int[][] positionDescription = getPositionDescriptionWithReverseInduction(child.ending);
                for (int i = 0; i < positionDescription.length; ++i) {
                    if (positionDescription[i][playerIndex] == max) {
                        bestPositions.add(positionDescription[i]);
                    }
                }
            }

            int[][] summaryPositionDescription = new int[bestPositions.size()][NUMBER_OF_PLAYERS];

            for (int j = 0; j < bestPositions.size(); ++j) {
                summaryPositionDescription[j] = bestPositions.get(j);
            }

            root.setPositionDescription(summaryPositionDescription);
            return summaryPositionDescription;
        }

        private static int[][] generatePositionDescription(int boundaries) {
            int[][] positionDescription = new int[1][NUMBER_OF_PLAYERS];
            Random random = new Random();
            positionDescription[0][0] = random.nextInt(boundaries);
            positionDescription[0][1] = random.nextInt(boundaries);
            positionDescription[0][2] = random.nextInt(boundaries);
            System.out.println(arrayToString(positionDescription));
            return positionDescription;
        }
    }

    private static class InnerNode {
        private int name;
        private int depth;
        private InnerNode parent;
        private int[][] positionDescription = new int[1][NUMBER_OF_PLAYERS];
        private List<Edge> children;

        InnerNode(InnerNode parent, int name, int depth) {
            this.name = name;
            this.parent = parent;
            this.depth = depth;
            this.children = new LinkedList<>();
        }

        void addChildren(InnerNode child) {
            Edge edge = new Edge(this, child);
            this.children.add(edge);
        }

        List<Edge> getChildren() {
            return children;
        }

        InnerNode getParent() {
            return parent;
        }

        int getName() {
            return name;
        }

        void setPositionDescription(int[][] description) {
            this.positionDescription = description;
        }

        int[][] getPositionDescription() {
            return positionDescription;
        }

        int getDepth() {
            InnerNode iterableNode = this;
            int depth = 0;
            while (iterableNode.getParent() != null) {
                depth++;
                iterableNode = iterableNode.getParent();
            }
            return depth;
        }

    }

    private static class Edge {
        private InnerNode beginning;
        private InnerNode ending;
        private Color color;

        Edge(InnerNode beginning, InnerNode ending) {
            this.beginning = beginning;
            this.ending = ending;
            this.color = Color.BLACK;
        }

        void setColor(Color color) {
            this.color = color;
        }

        InnerNode getBeginning() {
            return beginning;
        }

        InnerNode getEnding() {
            return ending;
        }

        Color getColor() {
            return color;
        }
    }

}