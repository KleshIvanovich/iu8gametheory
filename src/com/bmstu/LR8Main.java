package com.bmstu;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

public class LR8Main {

    private final static double LOCALITY_SIZE = Math.sqrt(2);
    private final static double MAIN_SPHERE_SIZE = 1;
    private final static double NUMBER_OF_A_POINTS = 1;

    private final static double EPS = 0.00001;
    private final static int LIMIT_OF_ITERATIONS = 1000000;

    private static LinkedList<Double> lastResults = new LinkedList<>();

    public static void main(String[] args) {
        solveGameOnSphereAnalytically(MAIN_SPHERE_SIZE, NUMBER_OF_A_POINTS, LOCALITY_SIZE);
        solveGameNumerical(MAIN_SPHERE_SIZE, NUMBER_OF_A_POINTS, LOCALITY_SIZE, 50);
    }

    // Петросян, Шевкопляс Теория Игр страница 96
    private static void solveGameOnSphereAnalytically(double mainSphereSize,
                                                      double numberOfPoints,
                                                      double localitySize) {
        double costOfTheGame = numberOfPoints * 0.5 * (1 - Math.sqrt(1 - Math.pow(localitySize / mainSphereSize, 2)));
        System.out.println("Цена игры:" + costOfTheGame);
    }

    private static void solveGameNumerical(double mainSphereSize,
                                           double numberOfPoints,
                                           double localitySize,
                                           int convergenceLimit) {
        double playerAWins = 0;
        int iterations = 0;

        do {
            if (lastResults.size() > convergenceLimit) {
                lastResults.remove(0);
            }
            if (playRandomGame(mainSphereSize, numberOfPoints, localitySize)) {
                playerAWins += 1;
            }
            iterations++;
            double currentAScore = playerAWins / iterations;
            lastResults.add(currentAScore);

            double max = Collections.max(lastResults);
            double min = Collections.min(lastResults);

            if (max != min && Math.abs(Math.abs(max) - Math.abs(min)) < EPS) {
                System.out.println("Цена игры (численное решение): " + playerAWins / iterations);
                System.out.println("Число итераций: " + iterations);

                return;
            }
        } while (iterations < LIMIT_OF_ITERATIONS);
        System.out.println("Лимит операций превышен, текущая цена игры:" + playerAWins / iterations);
    }

    private static boolean playRandomGame(double mainSphereSize,
                                          double numberOfPoints,
                                          double localitySize) {
        LinkedList<Point> playerAPoints = new LinkedList<>();
        for (int i = 0; i < numberOfPoints; ++i) {
            playerAPoints.add(generateRandomPointOnSphere(mainSphereSize));
        }

        Point playerBPoint = generateRandomPointOnSphere(mainSphereSize);
        boolean isPlayerBSpotted = false;

        for (Point point : playerAPoints) {
            if (point.distanceFrom(playerBPoint) < localitySize) {
                isPlayerBSpotted = true;
            }
        }
        return isPlayerBSpotted;
    }

    private static Point generateRandomPointOnSphere(double radius) {
        double x1 = (new Random()).nextGaussian();
        double x2 = (new Random()).nextGaussian();
        double x3 = (new Random()).nextGaussian();

        double lengthOfVector = Math.sqrt(Math.pow(x1, 2) +
                Math.pow(x2, 2) +
                Math.pow(x3, 2));
        if (lengthOfVector == 0d) {
            // Avoid dividing by zero.
            return generateRandomPointOnSphere(radius);
        }
        return new Point((x1*radius) / lengthOfVector,
                (x2*radius)  / lengthOfVector ,
                (x3*radius)  / lengthOfVector );
    }

    private static Point generateRandomPointV2(double radius) {
        double z = 2*(new Random().nextDouble()) - 1;
        double angle = Math.toRadians(360 * (new Random().nextDouble()));

        double x = Math.sqrt(1 - Math.pow(z, 2))*Math.cos(angle);
        double y = Math.sqrt(1 - Math.pow(z, 2))*Math.sin(angle);

        return new Point(x*radius, y*radius, z*radius);
    }

    private static class Point {
        private final double x;
        private final double y;
        private final double z;

        Point(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        double x() {
            return x;
        }

        double y() {
            return y;
        }

        double z() {
            return z;
        }

        double distanceFrom(Point point) {
            return Math.sqrt(Math.pow(this.x - point.x(), 2) +
                    Math.pow(this.y - point.y(), 2) +
                    Math.pow(this.z - point.z(), 2));
        }
    }
}
