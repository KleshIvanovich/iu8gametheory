package com.bmstu;

interface MixedGameStrategy {

    interface Results {
        double[] getStrategyForX();

        double[] getStrategyForY();

        double getCost();
    }

    Results solution();
}
