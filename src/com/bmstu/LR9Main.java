package com.bmstu;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class LR9Main {
    private static final double BOUNDARIES = 50;
    private static final int MATRIX_SIZE = 10;
    private static final double[][][] CROSSROAD_BI_MATRIX
            = {{{1, 1}, {0.5, 2}}, {{2, 0.5}, {0, 0}}};

    private static final double[][][] BATTLE_OF_MALES_BI_MATRIX
            = {{{4, 1}, {0, 0}}, {{0, 0}, {1, 4}}};

    private static final double[][][] PRISONER_DILEMMA_BI_MATRIX
            = {{{-5, -5}, {0, -10}}, {{-10, 0}, {-1, -1}}};

    private static final double[][][] INPUT_BI_MATRIX = {{{6, 7}, {8, 4}}, {{2, 1}, {9, 3}}};

    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_BLUE = "\u001B[34m";

    public static void main(String[] args) {
        System.out.println(ANSI_RED + "Равновесие по Нэшу" + ANSI_RESET);
        System.out.println(ANSI_BLUE + "Парето эффективность" + ANSI_RESET);
        System.out.println(ANSI_GREEN + "Все вместе" + ANSI_RESET);


        System.out.println("Перекресток:");
        findAllOptimalSituations(CROSSROAD_BI_MATRIX);
        System.out.println("Семейный спор:");
        findAllOptimalSituations(BATTLE_OF_MALES_BI_MATRIX);
        System.out.println("Дилемма заключенного:");
        findAllOptimalSituations(PRISONER_DILEMMA_BI_MATRIX);

        double[][][] inputMatrix = generateBiMatrix(MATRIX_SIZE, BOUNDARIES);

        System.out.println();
        System.out.println("Случайно сгенерированная матрица 10х10:");
        findAllOptimalSituations(inputMatrix);

        System.out.println("Матрица по варианту");
        findAllOptimalSituations(INPUT_BI_MATRIX);
        solveAnalitycally(INPUT_BI_MATRIX);
    }

    private static double[][][] generateBiMatrix(int size, double boundaries) {
        double[][][] matrix = new double[size][size][2];
        double min = (-1) * boundaries;
        double max = boundaries;

        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix.length; ++j) {
                matrix[i][j][0] = ThreadLocalRandom.current().nextInt((int)min, (int)max);
                matrix[i][j][1] = ThreadLocalRandom.current().nextInt((int)min, (int)max);
            }
        }

        return matrix;
    }

    private static void findAllOptimalSituations(double[][][] inputMatrix) {
        NumberFormat format = new DecimalFormat("#0.0");

        for (int i = 0; i < inputMatrix.length; ++i) {
            for (int j = 0; j < inputMatrix[0].length; ++j) {
                boolean isNashEfficient = checkForNashOptimality(inputMatrix, i, j);
                boolean isParetoOptimal = checkForParretoEfficiency(inputMatrix, i, j);
                String result = "(" + format.format(inputMatrix[i][j][0]) + ";"
                        + format.format(inputMatrix[i][j][1]) + ")";
                if (isNashEfficient && isParetoOptimal) {
                    result = ANSI_GREEN + result + ANSI_RESET;
                }
                if (isNashEfficient && !isParetoOptimal) {
                    result = ANSI_RED + result + ANSI_RESET;
                }

                if (!isNashEfficient && isParetoOptimal) {
                    result = ANSI_BLUE + result + ANSI_RESET;
                }

                System.out.print(result + " ");
            }
            System.out.println();
        }
    }

    private static boolean checkForNashOptimality(double[][][] matrix, int i, int j) {
        boolean isThereBetterStrategyForA = false;
        boolean isThereBetterStrategyForB = false;
        for (int jIter = 0; jIter < matrix.length; ++jIter) {
            if ((matrix[i][jIter][1] > matrix[i][j][1])) {
                isThereBetterStrategyForA = true;
            }
        }

        for (int iter = 0; iter < matrix.length; ++iter) {
            if ((matrix[iter][j][0] > matrix[i][j][0])) {
                isThereBetterStrategyForB = true;
            }
        }

        return !(isThereBetterStrategyForA | isThereBetterStrategyForB);
    }

    private static boolean checkForParretoEfficiency(double[][][] matrix, int i, int j) {
        boolean isThereBetterStrategy = false;

        for (int iter = 0; iter < matrix.length; ++iter) {
            for (int jIter = 0; jIter < matrix.length; ++jIter) {
                if ((matrix[iter][jIter][0] > matrix[i][j][0] &&
                        matrix[iter][jIter][1] >= matrix[i][j][1]) ||
                        (matrix[iter][jIter][1] > matrix[i][j][1] &&
                                matrix[iter][jIter][0] >= matrix[i][j][0])) {
                    isThereBetterStrategy = true;
                }
            }
        }

        return !(isThereBetterStrategy);
    }

    private static void solveAnalitycally(double[][][] matrix) {

        double[][] matrixA = new double[matrix.length][matrix.length];
        double[][] matrixB = new double[matrix.length][matrix.length];

        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix.length; ++j) {
                matrixA[i][j] = matrix[i][j][0];
                matrixB[i][j] = matrix[i][j][1];
            }
        }

        double[] uRow = new double[matrix.length];
        for (int i = 0; i < matrix.length; ++i) {
            uRow[i] = 1;
        }
        double[][] u = {uRow};
        double[][] u_t = new double[matrix.length][1];
        for (int i = 0; i < matrix.length; ++i) {
            u_t[i][0] = 1;
        }

        double[][] aReverse = MatrixTools.calculateReverseMatrix(matrixA);
        double[][] bReverse = MatrixTools.calculateReverseMatrix(matrixB);

        double[][] aReverseUT = MatrixTools.multiplicate(aReverse, u_t);
        double[][] bReverseUT = MatrixTools.multiplicate(bReverse, u_t);

        double vX = 1/MatrixTools.multiplicate(u, aReverseUT)[0][0];
        double vY = 1/MatrixTools.multiplicate(u, bReverseUT)[0][0];
        NumberFormat format = new DecimalFormat("#0.000");

        System.out.println("VX:" + format.format(vX));
        System.out.println("VY:" + format.format(vY));

        double[] x = {MatrixTools.multiplicate(aReverse, u_t)[0][0]*vX, MatrixTools.multiplicate(aReverse, u_t)[1][0]*vX};
        double[] y = {MatrixTools.multiplicate(u, bReverse)[0][0]*vY, MatrixTools.multiplicate(u, bReverse)[0][1]*vY};

        System.out.println("X:" + Arrays.toString(x));
        System.out.println("Y:" + Arrays.toString(y));
    }

//    private void checkIfDominating(double[][] matrix) {
//        for (int i = 0; i < matrix.length; ++i) {
//            for (int iIter = 0; )
//        }
//
//        for (int j = 0; i < matrix.length; ++j)
//    }
}
