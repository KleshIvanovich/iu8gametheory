package com.bmstu;

import Jama.Matrix;

import java.util.ArrayList;
import java.util.Random;

public class LR12Main {

    private static final double EPS = 1e-6;
    private static final int size = 9;
    private static final int boundaries = 20;

    public static void main(String[] args) {
        System.out.println("Матрица доверия");

        double[][] trustMatrix = generateTrustMatrix(size);
        new Matrix(trustMatrix).print(3, 3);

        System.out.println("Начальные мнения");

        double[][] originalOpinionVector = generateOpinionVector(size, boundaries);
        new Matrix(originalOpinionVector).print(3, 3);
        findFinalStateVector(trustMatrix, originalOpinionVector);

        System.out.println("Моделирование игры с информационным влиянием:");
        double[][] influenceGameVector = generateInfluenceGameVector(size, 100);
        new Matrix(influenceGameVector).print(3, 3);
        findFinalStateVector(trustMatrix, influenceGameVector);
    }

    private static void findFinalStateVector(double[][] matrix, double[][] vector) {
        double[][] finalVector = vector.clone();
        double[][] matrixInf = matrix.clone();
        int iteration = 0;
        do {
            double[][] newOpinionVector = MatrixTools.multiplicate(matrix, finalVector);
            matrixInf = MatrixTools.multiplicate(matrixInf, matrix);
            iteration++;
            if (isOpinionVectorAcceptable(newOpinionVector, EPS)) {
                break;
            }
            finalVector = newOpinionVector;
        } while (true);
        System.out.println("Итерация:" + iteration);
        System.out.println("Вектор финальных мнений");
        new Matrix(finalVector).print(3, 3);

        System.out.println("Результирующая матрица:");
        new Matrix(matrixInf).print(3,3);
    }

    private static boolean isOpinionVectorAcceptable(double[][] opinionVector, double eps) {
        boolean isAcceptable = true;
        for (int i = 0; i < opinionVector.length; ++i) {
            double currentOpinion = opinionVector[i][0];
            for (int j = 0; j < opinionVector.length; ++j) {
                if (Math.abs(Math.abs(currentOpinion) - Math.abs(opinionVector[j][0])) > eps) {
                    return false;
                }
            }
        }
        return isAcceptable;
    }

    private static double[][] generateOpinionVector(int size, int boundaries) {
        double[][] matrix = new double[size][0];
        Random random = new Random();
        for (int i = 0; i < size; ++i) {
            double[] vector = new double[1];
            vector[0] = (double) random.nextInt(boundaries) + 1;
            matrix[i] = vector;
        }

        return matrix;
    }

    private static double[][] generateInfluenceGameVector(int size, int boundaries) {
        Random random = new Random();

        int playerAAgentsQuantity = random.nextInt(size);
        int playerBAgentsQuantity = random.nextInt(size);

        if (playerAAgentsQuantity + playerBAgentsQuantity >= size) {
            boolean isPlayerAFirst = random.nextBoolean();
            if (isPlayerAFirst) {
                playerBAgentsQuantity = size - playerAAgentsQuantity;
            } else {
                playerAAgentsQuantity = size - playerBAgentsQuantity;
            }
        }

        Random influenceRandom = new Random();
        double playerAInfluence = (double) influenceRandom.nextInt(boundaries);
        double playerBInfluence = (-1) * (double) influenceRandom.nextInt(boundaries);

        ArrayList<Integer> playerAAgents = new ArrayList<>();
        ArrayList<Integer> playerBAgents = new ArrayList<>();

        for (int i = 0; i < playerAAgentsQuantity; ++i) {
            playerAAgents.add(random.nextInt(size));
        }

        for (int i = 0; i < playerBAgentsQuantity; ++i) {
            do {
                int possibleAgent = random.nextInt(size);
                if (playerAAgents.contains(possibleAgent)) {
                    continue;
                }
                playerBAgents.add(possibleAgent);
                break;
            } while (true);
        }

        double[][] matrix = new double[size][0];
        for (int i = 0; i < size; ++i) {
            double[] vector = new double[1];
            if (playerAAgents.contains(i)) {
                vector[0] = playerAInfluence;
            } else if (playerBAgents.contains(i)) {
                vector[0] = playerBInfluence;
            } else {
                vector[0] = (double) influenceRandom.nextInt(boundaries / 4);
            }

            matrix[i] = vector;
        }

        return matrix;

    }

    private static double[][] generateTrustMatrix(int size) {
        double[][] matrix = new double[size][size];
        Random random = new Random();
        for (int i = 0; i < size; ++i) {
            double sumInRow = 0;
            for (int j = 0; j < size; ++j) {
                matrix[i][j] = 30 * random.nextDouble();
                sumInRow += matrix[i][j];
            }

            for (int j = 0; j < size; ++j) {
                matrix[i][j] /= sumInRow;
            }
        }
        return matrix;
    }
}
