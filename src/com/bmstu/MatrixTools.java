package com.bmstu;

import Jama.Matrix;

public final class MatrixTools {
    public static double[][] multiplicate(double[][] A, double[][] B) {
        int aRows = A.length;
        int aColumns = A[0].length;
        int bRows = B.length;
        int bColumns = B[0].length;

        if (aColumns != bRows) {
            throw new IllegalArgumentException("A:Rows: " + aColumns +
                              " does not match B:Columns " + bRows + ".");
        }

        double[][] C = new double[aRows][bColumns];
        for (int i = 0; i < aRows; i++) {
            for (int j = 0; j < bColumns; j++) {
                C[i][j] = 0.00000;
            }
        }

        for (int i = 0; i < aRows; i++) { // aRow
            for (int j = 0; j < bColumns; j++) { // bColumn
                for (int k = 0; k < aColumns; k++) { // aColumn
                    C[i][j] += A[i][k] * B[k][j];
                }
            }
        }
        return C;
    }

    public static double[][] multiplicateByConst(double[][] A, double value) {
        double[][] C = new double[A.length][A[0].length];

        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[0].length; j++) {
                C[i][j] = A[i][j] * value;
            }
        }
        return C;
    }


    public static double[][] calculateReverseMatrix(double[][] inputMatrix) {
        if (inputMatrix.length != inputMatrix[0].length) {
            throw new IllegalArgumentException("Matrix is not squared");
        }
        return new Matrix(inputMatrix).inverse().getArray();
    }
}
