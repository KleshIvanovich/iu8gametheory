package com.bmstu;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class BrawnRobinson implements MixedGameStrategy {

    private double[][] gameMatrix;
    private final double eps;

    private List<Double> Vmax;
    private List<Double> Vmin;

    private int iteration;

    BrawnRobinson(double[][] gameMatrix, double eps) {
        this.gameMatrix = gameMatrix;
        this.eps = eps;
    }

    @Override
    public Results solution() {
        return brawnRobinsonSolution();
    }

    private Results brawnRobinsonSolution() {

        Vmax = new ArrayList<>();
        Vmin = new ArrayList<>();

        // Contains quantity of chooses
        Double[] playerOneChooses = toPrimitive(new ArrayList<>(gameMatrix.length),
                gameMatrix.length);
        Double[] playerTwoChooses = toPrimitive(new ArrayList<>(gameMatrix[0].length),
                gameMatrix[0].length);

        // Contains gain over iterations
        Double[] playerOneGain = toPrimitive(new ArrayList<>(gameMatrix.length), gameMatrix.length);
        Double[] playerTwoLoss = toPrimitive(new ArrayList<>(gameMatrix[0].length), gameMatrix[0].length);

        Random random = new Random();

        int lastYChoose;
        int lastXChoose;

        lastXChoose = random.nextInt(gameMatrix.length);
        playerOneChooses[lastXChoose] += 1;

        lastYChoose = random.nextInt(gameMatrix[0].length);
        playerTwoChooses[lastYChoose] += 1;

        iteration = 0;
        // For init loop
        double currentEps = 0;

        do {

            iteration++;

            lastXChoose = bestCleanStrategyForX((playerOneGain), lastYChoose);
            lastYChoose = bestCleanStrategyForY((playerTwoLoss), lastXChoose);
            playerOneChooses[lastXChoose]++;
            playerTwoChooses[lastYChoose]++;
            NumberFormat format = new DecimalFormat("#0.000");
            currentEps = updateEps();
            System.out.println(iteration + ";" + lastXChoose + ";" + lastYChoose + ";" + Arrays.toString(playerOneGain)
                    + ";" + Arrays.toString(playerTwoLoss) + ";" + format.format(getMinFromDoubleArray(Vmax)) + ";"
                    + format.format(getMaxFromDoubleArray(Vmin)) + ";" + format.format(currentEps));
        } while (currentEps > eps);

        final Double[] strategyForX = playerOneChooses;
        final Double[] strategyForY = playerTwoChooses;

        final double totalRound = (double) iteration;

        return new Results() {
            @Override
            public double[] getStrategyForX() {
                return multiplicateByConst(strategyForX, 1 / totalRound);
            }

            @Override
            public double[] getStrategyForY() {
                return multiplicateByConst(strategyForY, 1 / totalRound);
            }

            @Override
            public double getCost() {
                return 0.5 * (getMinFromDoubleArray(Vmax) + getMaxFromDoubleArray(Vmin));
            }
        };
    }

    private Double[] toPrimitive(List<Double> array, int size) {
        if (array == null) {
            return null;
        }

        for (int i = 0; i < size; ++i) {
            array.add(0d);
        }
        final Double[] result = new Double[array.size()];
        for (int i = 0; i < array.size(); i++) {
            result[i] =  array.get(i);
        }
        return result;
    }

    private double updateEps() {
        return (getMinFromDoubleArray(Vmax) - getMaxFromDoubleArray(Vmin));
    }

    private Double getMaxFromDoubleArray(List<Double> array) {
        return Collections.max(array);
    }

    private Double getMinFromDoubleArray(List<Double> array) {
        return Collections.min(array);
    }

    private double[] multiplicateByConst(Double[] inputVector, double value) {
        double[] result = new double[inputVector.length];
        for (int i = 0; i < inputVector.length; ++i) {
            result[i] = value * inputVector[i];
        }
        return result;
    }

    /**
     * Here i need get last choose of other player, and choose optimal strategy for it,
     * then need to update current gain for player
     **/
    private int bestCleanStrategyForX(Double[] gainForX, int lastYChoose) {
        Double max = (-1) * Double.MAX_VALUE;
        int bestCleanStrategy = -1;
        for (int i = 0; i < gameMatrix.length; ++i) {
            gainForX[i] += gameMatrix[i][lastYChoose];
            if (gainForX[i] > max) {
                max = gainForX[i];
                bestCleanStrategy = i;
            }

            if (gainForX[i] == max) {
                Random random = new Random();
                bestCleanStrategy = (random.nextInt(1) == 1) ? bestCleanStrategy : i;
            }
        }

        Vmax.add(max/iteration);
        return bestCleanStrategy;
    }

    private int bestCleanStrategyForY(Double[] lossForY, int lastXChoose) {
        Double min = Double.MAX_VALUE;
        int bestCleanStrategy = -1;
        for (int i = 0; i < gameMatrix.length; ++i) {
            lossForY[i] += gameMatrix[lastXChoose][i];
            if (lossForY[i] < min) {
                min = lossForY[i];
                bestCleanStrategy = i;
            }
            if (lossForY[i] == min) {
                Random random = new Random();
                bestCleanStrategy = (random.nextInt(1) == 1) ? bestCleanStrategy : i;
            }
        }
        Vmin.add(min/iteration);
        return bestCleanStrategy;
    }
}



